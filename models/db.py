# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    db = DAL('sqlite://storage.sqlite',pool_size=1,check_reserved=['all'])
else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()

## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' or 'smtp.gmail.com:587'
mail.settings.sender = 'you@gmail.com'
mail.settings.login = 'username:password'

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.rpx_account import use_janrain
use_janrain(auth, filename='private/janrain.key')

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

db.define_table('semestre',
    Field('semester_name','string',notnull=True))
    
db.define_table('secciones',
    Field('seccion_name','string',notnull=True))
    
db.define_table('horarios',
    Field('horario_entrada','time',notnull=True),
    Field('horario_salida','time',notnull=True),
    Field('horario_dia','string',notnull=True))
    
db.define_table('universidades',
    Field('universidad_name','string',notnull=True),
    Field('universidad_address','string',notnull=True))  

db.define_table('facultades',            
    Field('facultad_name','string',notnull=True),
    Field('universidad_id',db.universidades))

db.define_table('departamentos',
    Field('departamento_name','string',notnull=True),
    Field('facultad_id',db.facultades))
    
db.define_table('cursos',
    Field('curso_code','string',notnull=True),
    Field('curso_name','string',notnull=True),
    Field('curso_description','text',notnull=True),
    Field('departamento_id',db.departamentos))
    
db.define_table('profesores',
    Field('profesor_name','string',notnull=True),
    Field('profesor_lastname','string',notnull=True),
    Field('departamento_id',db.departamentos))
    
db.define_table('cursos_profesores',
    Field('profesor_id',db.profesores),
    Field('semestre_id',db.semestre),
    Field('curso_id',db.cursos),
    Field('cursos_profesores_year','integer',notnull=True))
    
db.define_table('cursos_disponibles',
    Field('curso_profesor',db.cursos_profesores),
    Field('horario',db.horarios),
    Field('seccion',db.secciones))
    
db.define_table('evaluaciones',
    Field('disciplina','string',notnull=True),
    Field('enfoque_de_evaluacion','string',notnull=True),
    Field('disponibilidad','string',notnull=True),
    Field('dominio_del_material','string',notnull=True),
    Field('habilidad_de_explicar_el_material','string',notnull=True),
    Field('flexibilidad','string',notnull=True),
    Field('awesomeness','string',notnull=True),
    Field('good_looking','string',notnull=True),
    Field('curso_profesor',db.cursos_profesores),
    Field('auth_user_id',db.auth_user),
    Field('timecreated','datetime',notnull=True))
    
    
## after defining tables, uncomment below to enable auditing
## auth.enable_record_versioning(db)
